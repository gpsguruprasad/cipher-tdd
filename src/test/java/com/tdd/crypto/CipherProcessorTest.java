package com.tdd.crypto;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class CipherProcessorTest {
    @Test
    public void should_encrypt_with_1_rotation_1Char(){
        CipherProcessor cipher = new CipherProcessor();
        assertEquals("B", cipher.encrypt("A", 1));
    }

    @Test
    public void should_encrypt_with_2_rotation_1Char(){
        CipherProcessor cipher = new CipherProcessor();
        assertEquals("C", cipher.encrypt("A", 2));
    }

    @Test
    public void should_encrypt_with_4_rotation_1Char(){
        CipherProcessor cipher = new CipherProcessor();
        assertEquals("E", cipher.encrypt("A", 4));
    }

    @Test
    public void should_encrypt_with_1_rotation_String(){
        CipherProcessor cipher = new CipherProcessor();
        assertEquals("BCDEF", cipher.encrypt("ABCDE", 1));
    }

    @Test
    public void should_encrypt_with_1_rotation_string_over_z(){
        CipherProcessor cipher = new CipherProcessor();
        assertEquals("ABC", cipher.encrypt("XYZ", 3));
    }
}
