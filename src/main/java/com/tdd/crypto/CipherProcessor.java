package com.tdd.crypto;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CipherProcessor {
    public String encrypt(String input, int key) {
        StringBuilder encryptedString = new StringBuilder();

        for(int i = 0;i < input.length();i++){
            encryptedString.append(getCharForNumber(getNumberForChar(input.charAt(i))+key));
        }

        return encryptedString.toString();
    }

    private int getNumberForChar(char c) {
        return (int)c - 64;
    }

    private char getCharForNumber(int i) {
        return (char)(i + 64);
    }
}
